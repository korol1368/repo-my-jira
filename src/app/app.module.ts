import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatListModule} from '@angular/material/list';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from '@angular/material/snack-bar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {HttpClientModule} from '@angular/common/http';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import {AppComponent} from './app.component';
import {HomePageComponent} from './home-page/home-page.component';
import {ProjectFormComponent} from './home-page/project-form/project-form.component';
import {ProjectService} from './services/project.service';
import {ProjectPageComponent} from './project-page/project-page.component';
import {TaskFormComponent} from './project-page/task-form/task-form.component';
import {TaskService} from './services/task.service';
import {TaskListComponent} from './project-page/task-list/task-list.component';
import {TaskItemComponent} from './project-page/task-list/task-item/task-item.component';
import {ConfirmDeleteComponent} from './project-page/task-list/confirm-delete/confirm-delete.component';
import {FilterPipe} from './pipes/filter.pipe';
import {DataService} from './services/data.service';
import {AuthService} from './services/auth.service';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {NavbarComponent} from './navbar/navbar.component';
import {ProfileComponent} from './profile/profile.component';
import {AuthGuard} from './services/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    HomePageComponent,
    ProjectFormComponent,
    ProjectPageComponent,
    TaskFormComponent,
    TaskListComponent,
    TaskItemComponent,
    ConfirmDeleteComponent,
    FilterPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatListModule,
    MatSnackBarModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    DragDropModule,
    MatProgressSpinnerModule,
  ],
  providers: [
    DataService,
    ProjectService,
    TaskService,
    AuthService,
    AuthGuard,
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        duration: 2500,
        panelClass: ['snackbar-success'],
      },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
