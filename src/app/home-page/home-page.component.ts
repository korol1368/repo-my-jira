import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';
import {take} from 'rxjs/operators';

import {ProjectFormComponent} from './project-form/project-form.component';
import {ProjectInterface} from '../types/project.interface';
import {ProjectService} from '../services/project.service';
import {ConfirmDeleteComponent} from '../project-page/task-list/confirm-delete/confirm-delete.component';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  title = 'Ваши проекты';
  projectList: ProjectInterface[] = [];
  authorized!: boolean;

  constructor(
    private dialog: MatDialog,
    private authService: AuthService,
    private projectService: ProjectService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.authorized = this.authService.isAuthenticated();
    this.projectService.getList().subscribe((projects) => {
      if (projects) {
        this.projectList = projects;
      }
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ProjectFormComponent, {
      minWidth: '30vw',
      data: {},
    });

    dialogRef.afterClosed().subscribe((project) => {
      if (project) {
        this.projectService.addProject(project).subscribe((project) => {
          this.router.navigate([`/project/${project.id}`]);
          this.snackBar.open('Project added');
        });
      }
    });
  }

  delete(project: ProjectInterface): void {
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '50vw',
      data: {project},
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.projectService
          .delete(project)
          .pipe(take(1))
          .subscribe((response) => {
            if (response) {
              this.snackBar.open('Project deleted');
            }
          });
      }
    });
  }

  onGo(project: ProjectInterface): void {
    this.router.navigate([`/project/${project.id}`]);
  }
}
