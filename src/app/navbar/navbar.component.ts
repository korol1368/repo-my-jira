import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';

import {AuthService} from '../services/auth.service';
import {ProjectService} from '../services/project.service';
import {TaskService} from '../services/task.service';
import {ProjectInterface} from '../types/project.interface';
import {TaskFormComponent} from '../project-page/task-form/task-form.component';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  authorized!: boolean;
  project!: ProjectInterface;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private dialog: MatDialog,
    private taskService: TaskService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.authorized = this.authService.isAuthenticated();

    this.route.params.subscribe((params: Params) => {
      this.projectService.getOne(+params.id).subscribe((result) => {
        if (result) {
          this.project = result;
        }
      });
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(TaskFormComponent, {
      minWidth: '30vw',
      data: {},
    });

    dialogRef
      .afterClosed()
      .pipe(filter((item) => item !== null))
      .subscribe((task) => {
        this.taskService.addTask(task).subscribe((result) => {
          if (result) {
            this.snackBar.open('Task added');
          }
        });
      });
  }

  logout(): void {
    this.authService.logout().subscribe(() => {
      this.router.navigateByUrl('login');
    });
  }
}
