import {Pipe, PipeTransform} from '@angular/core';

import {TaskInterface} from '../types/task.interface';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(tasks: TaskInterface[], search: string): TaskInterface[] {
    if (!search) {
      return tasks;
    }
    return tasks.filter((task) => task.title.toLowerCase().includes(search.trim().toLowerCase()));
  }
}
