import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {take} from 'rxjs/operators';

import {AuthService} from '../services/auth.service';
import {ConfirmDeleteComponent} from '../project-page/task-list/confirm-delete/confirm-delete.component';
import {UserInterface} from '../types/user.interface';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  authorized!: boolean;
  userEmail = '';
  user!: UserInterface;

  constructor(
    private authService: AuthService,
    public dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.authorized = this.authService.isAuthenticated();

    this.authService.lookupUser().subscribe((user) => {
      this.userEmail = user.users[0].email;
      this.user = user.users[0];
    });
  }

  onClickDelete(user: UserInterface): void {
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '50vw',
      data: {user},
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      if (result) {
        this.authService
          .delete()
          .pipe(take(1))
          .subscribe((response) => {
            if (response) {
              this.router.navigate(['/login']);
              this.snackBar.open('Profile deleted');
            }
          });
      }
    });
  }
}
