import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup} from '@angular/forms';

import {DialogData} from '../../types/dialog-data.interface';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.scss'],
})
export class TaskFormComponent implements OnInit {
  form: FormGroup;
  title = '';
  description = '';
  author = '';
  executor = '';
  isAdd = true;
  id!: number;

  constructor(public dialogRef: MatDialogRef<TaskFormComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    if (data.task) {
      this.isAdd = false;
      this.id = data.task.id;
      this.title = data.task.title;
      this.description = data.task.description;
      this.author = data.task.author;
      this.executor = data.task.executor;
    }

    this.form = new FormGroup({
      title: new FormControl(null),
      description: new FormControl(null),
      author: new FormControl(null),
      executor: new FormControl(null),
    });
  }

  ngOnInit(): void {}

  onNoClick(): void {
    this.dialogRef.close(null);
  }

  onSubmit(): void {
    this.dialogRef.close(this.form.value);
  }
}
