import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

import {DialogData} from '../../../types/dialog-data.interface';

@Component({
  selector: 'app-confirm-delete-task',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.scss'],
})
export class ConfirmDeleteComponent {
  title = '';
  name = '';
  isProfile = false;
  isTask = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    if (data.task) {
      this.isTask = true;
      this.isProfile = false;
      this.title = data.task.title;
    } else if (data.project) {
      this.isTask = false;
      this.isProfile = false;
      this.name = data.project.name;
    } else if (data.user) {
      this.isProfile = true;
    }
  }
}
