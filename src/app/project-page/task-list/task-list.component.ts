import {Component, OnDestroy, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {take} from 'rxjs/operators';
import {Subscription} from 'rxjs';

import {TaskService} from '../../services/task.service';
import {TaskInterface} from '../../types/task.interface';
import {TaskFormComponent} from '../task-form/task-form.component';
import {ConfirmDeleteComponent} from './confirm-delete/confirm-delete.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
})
export class TaskListComponent implements OnInit, OnDestroy {
  titleIncomplete = 'Incomplete';
  titleComplete = 'Complete';
  titleVerify = 'Verify';
  tasksIncomplete: TaskInterface[] = [];
  tasksComplete: TaskInterface[] = [];
  tasksVerify: TaskInterface[] = [];
  search!: string;
  incSab!: Subscription;
  comSab!: Subscription;
  verSab!: Subscription;

  constructor(private taskService: TaskService, private dialog: MatDialog, private snackBar: MatSnackBar) {}

  ngOnInit(): void {
    this.incSab = this.taskService.getIncomplete().subscribe((tasks) => {
      if (tasks) {
        this.tasksIncomplete = tasks;
      }
    });
    this.comSab = this.taskService.getComplete().subscribe((tasks) => {
      if (tasks) {
        this.tasksComplete = tasks;
      }
    });
    this.verSab = this.taskService.getVerify().subscribe((tasks) => {
      if (tasks) {
        this.tasksVerify = tasks;
      }
    });
  }

  drop(event: CdkDragDrop<TaskInterface[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      console.log(this.tasksComplete);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }
  }

  edit(task: TaskInterface): void {
    const dialogRef = this.dialog.open(TaskFormComponent, {
      minWidth: '50vw',
      data: {task},
    });

    dialogRef.afterClosed().subscribe((item) => {
      if (item) {
        const updatedTask: TaskInterface = {
          id: task.id,
          title: item.title,
          description: item.description,
          author: item.author,
          executor: item.executor,
        };

        this.taskService.update(updatedTask).subscribe(() => {
          this.snackBar.open('Task edited');
        });
      }
    });
  }

  delete(task: TaskInterface): void {
    const dialogRef = this.dialog.open(ConfirmDeleteComponent, {
      width: '50vw',
      data: {task},
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.taskService
          .delete(task)
          .pipe(take(1))
          .subscribe((response) => {
            if (response) {
              this.snackBar.open('Task deleted');
            }
          });
      }
    });
  }

  ngOnDestroy(): void {
    this.incSab.unsubscribe();
    this.comSab.unsubscribe();
    this.verSab.unsubscribe();
  }
}
