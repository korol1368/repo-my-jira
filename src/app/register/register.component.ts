import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthService} from '../services/auth.service';
import {UserInterface} from '../types/user.interface';
import {Validation} from '../../assets/utils/validation';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  hide = true;
  submitted!: boolean;
  message = '';

  constructor(public authService: AuthService, private router: Router, private snackBar: MatSnackBar) {
    this.form = new FormGroup(
      {
        email: new FormControl(null, [Validators.required, Validators.email]),
        password: new FormControl(null, [Validators.required]),
        confirmPassword: new FormControl(null, [Validators.required]),
      },
      {
        validators: [Validation.match('password', 'confirmPassword')],
      }
    );
  }

  ngOnInit(): void {}

  getFormControl(name: string): FormControl {
    return this.form.get(name) as FormControl;
  }

  onSubmit(): void {
    if (this.form.invalid) {
      return;
    }

    this.submitted = true;

    const user: UserInterface = {
      email: this.form.value.email,
      password: this.form.value.password,
    };

    this.authService.register(user).subscribe((result) => {
      if (result) {
        this.form.reset();
        this.router.navigate(['/login']);
        this.snackBar.open('User registered');
        this.submitted = false;
      }
    });
  }
}
