import {Injectable} from '@angular/core';
import {Observable, of, Subject, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

import {UserInterface} from '../types/user.interface';
import {DataService} from './data.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {FbAuthResponse} from '../types/fb-auth-response';
import {FirebaseAuthUserResponseInterface} from '../types/firebase-auth-user-response.interface';

@Injectable()
export class AuthService {
  public error$: Subject<string> = new Subject<string>();

  readonly TOKEN_KEY = 'fb-token';
  readonly TOKEN_EXP_KEY = 'fb-token-exp';

  constructor(private dataService: DataService, private http: HttpClient) {}

  lookupUser(): Observable<FirebaseAuthUserResponseInterface> {
    return this.http.post<FirebaseAuthUserResponseInterface>(
      `https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=${environment.apiKey}`,
      {
        idToken: localStorage.getItem(this.TOKEN_KEY),
      }
    );
  }

  get token(): string | null {
    const fbTokenExp = localStorage.getItem(this.TOKEN_EXP_KEY);
    if (fbTokenExp) {
      const expDate = new Date(fbTokenExp);
      if (new Date() > expDate) {
        this.logout();
        return null;
      }
    } else {
      this.logout();
      return null;
    }

    return localStorage.getItem(this.TOKEN_KEY);
  }

  register(user: UserInterface): Observable<any> {
    return this.http
      .post<FbAuthResponse>(`https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${environment.apiKey}`, user)
      .pipe(tap(this.setToken.bind(this)), catchError(this.handleError.bind(this)));
  }

  login(user: UserInterface): Observable<any> {
    user.returnSecureToken = true;
    return this.http
      .post<FbAuthResponse>(
        `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${environment.apiKey}`,
        user
      )
      .pipe(tap(this.setToken.bind(this)), catchError(this.handleError.bind(this)));
  }

  logout(): Observable<boolean> {
    localStorage.removeItem(this.TOKEN_KEY);
    localStorage.removeItem(this.TOKEN_EXP_KEY);

    return of(false);
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  private handleError(error: any): any {
    const {message} = error.error.error;
    switch (message) {
      case 'INVALID_EMAIL':
        this.error$.next('Invalid email');
        break;
      case 'INVALID_PASSWORD':
        this.error$.next('Invalid password');
        break;
      case 'EMAIL_NOT_FOUND':
        this.error$.next('There is no such email');
        break;
      case 'EMAIL_EXISTS':
        this.error$.next('Email busy');
    }
    return throwError(error);
  }
  //
  delete(): Observable<boolean> {
    this.http.post(`https://identitytoolkit.googleapis.com/v1/accounts:delete?key=${environment.apiKey}`, {
      idToken: localStorage.getItem(this.TOKEN_KEY),
    });

    this.logout();
    return of(true);
  }

  private setToken(response: FbAuthResponse): void {
    const expDate = new Date(new Date().getTime() + +response.expiresIn * 1000);
    localStorage.setItem(this.TOKEN_KEY, response.idToken);
    localStorage.setItem(this.TOKEN_EXP_KEY, expDate.toString());
  }
}
