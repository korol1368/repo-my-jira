import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from '../../environments/environment';
import {ProjectInterface} from '../types/project.interface';
import {TaskInterface} from '../types/task.interface';
import {UserInterface} from '../types/user.interface';

@Injectable()
export class DataService {
  constructor(private http: HttpClient) {}

  getProjects(): Observable<ProjectInterface[]> {
    return this.http.get<ProjectInterface[]>(environment.apiUrl.projects);
  }

  getTasksIncomplete(): Observable<TaskInterface[]> {
    return this.http.get<TaskInterface[]>(environment.apiUrl.tasks.tasksIncomplete);
  }

  getTasksComplete(): Observable<TaskInterface[]> {
    return this.http.get<TaskInterface[]>(environment.apiUrl.tasks.tasksComplete);
  }

  getTasksVerify(): Observable<TaskInterface[]> {
    return this.http.get<TaskInterface[]>(environment.apiUrl.tasks.tasksVerify);
  }
}
