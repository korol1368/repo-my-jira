import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {take} from 'rxjs/operators';

import {DataService} from './data.service';
import {ProjectInterface} from '../types/project.interface';

@Injectable()
export class ProjectService {
  private projects = new BehaviorSubject<ProjectInterface[]>([]);
  constructor(private dataService: DataService) {
    this.dataService
      .getProjects()
      .pipe(take(1))
      .subscribe((projects) => this.projects.next(projects));
  }

  getList(): Observable<ProjectInterface[]> {
    return this.projects.asObservable();
  }

  getOne(id: number): Observable<ProjectInterface | undefined> {
    const projects = this.projects.getValue();
    const one = projects.find((x) => x.id === id);
    return of(one);
  }

  addProject(name: string): Observable<ProjectInterface> {
    const projects = this.projects.getValue();
    const newProject: ProjectInterface = {
      id: this.getNextId(),
      name,
    };
    projects.push(newProject);
    this.projects.next(projects);

    return of(newProject);
  }

  protected getNextId(): number {
    const projects = this.projects.getValue();

    const lastId = Math.max.apply(
      Math,
      projects.map((o: ProjectInterface) => o.id)
    );

    return lastId + 1;
  }

  delete(project: ProjectInterface): Observable<boolean> {
    const projects = this.projects.getValue();
    const idx = projects.findIndex((x) => x.id === project.id);
    if (idx > -1) {
      projects.splice(idx, 1);
    }
    this.projects.next(projects);
    return of(true);
  }
}
