import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {TaskInterface} from '../types/task.interface';
import {DataService} from './data.service';
import {take} from 'rxjs/operators';

@Injectable()
export class TaskService {
  private tasksIncomplete = new BehaviorSubject<TaskInterface[]>([]);
  private tasksComplete = new BehaviorSubject<TaskInterface[]>([]);
  private tasksVerify = new BehaviorSubject<TaskInterface[]>([]);
  constructor(private dataService: DataService) {
    this.dataService
      .getTasksIncomplete()
      .pipe(take(1))
      .subscribe((tasks) => this.tasksIncomplete.next(tasks));

    this.dataService
      .getTasksComplete()
      .pipe(take(1))
      .subscribe((tasks) => this.tasksComplete.next(tasks));

    this.dataService
      .getTasksVerify()
      .pipe(take(1))
      .subscribe((tasks) => this.tasksVerify.next(tasks));
  }

  getIncomplete(): Observable<TaskInterface[]> {
    return this.tasksIncomplete.asObservable();
  }
  getComplete(): Observable<TaskInterface[]> {
    return this.tasksComplete.asObservable();
  }
  getVerify(): Observable<TaskInterface[]> {
    return this.tasksVerify.asObservable();
  }

  addTask(task: TaskInterface): Observable<TaskInterface> {
    const tasks = this.tasksIncomplete.getValue();
    const newTask: TaskInterface = {
      id: this.getNextId(),
      title: task.title,
      description: task.description,
      author: task.author,
      executor: task.executor,
    };
    tasks.push(newTask);
    this.tasksIncomplete.next(tasks);

    return of(newTask);
  }

  update(task: TaskInterface): Observable<TaskInterface> {
    const tasksIncomplete = this.tasksIncomplete.getValue();
    const idx = tasksIncomplete.findIndex((x) => x.id === task.id);

    tasksIncomplete[idx] = task;

    this.tasksIncomplete.next(tasksIncomplete);

    return of(task);
  }

  delete(task: TaskInterface): Observable<boolean> {
    const tasks = this.tasksVerify.getValue();
    const idx = tasks.findIndex((x) => x.id === task.id);
    if (idx > -1) {
      tasks.splice(idx, 1);
    }
    this.tasksVerify.next(tasks);
    return of(true);
  }

  protected getNextId(): number {
    const tasksIncomplete = this.tasksIncomplete.getValue();
    const tasksComplete = this.tasksComplete.getValue();
    const tasksVerify = this.tasksVerify.getValue();
    const tasks = tasksIncomplete.concat(tasksComplete, tasksVerify);

    const lastId = Math.max.apply(
      Math,
      tasks.map((o: TaskInterface) => o.id)
    );

    return lastId + 1;
  }
}
