import {TaskInterface} from './task.interface';
import {ProjectInterface} from './project.interface';
import {UserInterface} from './user.interface';

export interface DialogData {
  task: TaskInterface;
  project: ProjectInterface;
  user: UserInterface;
}
