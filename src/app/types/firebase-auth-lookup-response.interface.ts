export interface FirebaseAuthLookupResponseInterface {
  users: [
    {
      email: string;
    }
  ];
}
