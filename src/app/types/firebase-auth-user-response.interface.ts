export interface FirebaseAuthUserResponseInterface {
  users: [
    {
      createdAt: string;
      email: string;
      emailVerified: boolean;
      lastLoginAt: string;
      lastRefreshAt: string;
      localId: string;
      passwordHash: string;
      passwordUpdatedAt: number;
      validSince: string;
    }
  ];
}
