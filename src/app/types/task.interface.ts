export interface TaskInterface {
  id: number;
  title: string;
  description: string;
  author: string;
  executor: string;
}
