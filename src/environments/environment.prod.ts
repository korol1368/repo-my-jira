import {Environment} from './interface';

export const environment: Environment = {
  production: true,
  apiKey: 'AIzaSyBOyURB4GSMx3u-2L9IrMzxelvv1alai54',
  apiUrl: {
    projects: '/assets/mock-responses/projects/projects.json',
    tasks: {
      tasksIncomplete: '/assets/mock-responses/tasks/tasksIncomplete.json',
      tasksComplete: '/assets/mock-responses/tasks/tasksComplete.json',
      tasksVerify: '/assets/mock-responses/tasks/tasksVerify.json',
    },
  },
};
