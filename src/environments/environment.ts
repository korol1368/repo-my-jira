// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {Environment} from './interface';

export const environment: Environment = {
  production: false,
  apiKey: 'AIzaSyBOyURB4GSMx3u-2L9IrMzxelvv1alai54',
  apiUrl: {
    projects: '/assets/mock-responses/projects/projects.json',
    tasks: {
      tasksIncomplete: '/assets/mock-responses/tasks/tasksIncomplete.json',
      tasksComplete: '/assets/mock-responses/tasks/tasksComplete.json',
      tasksVerify: '/assets/mock-responses/tasks/tasksVerify.json',
    },
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
