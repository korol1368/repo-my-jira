export interface Environment {
  production: boolean;
  apiKey: string;
  apiUrl: {
    projects: string;
    tasks: {
      tasksIncomplete: string;
      tasksComplete: string;
      tasksVerify: string;
    };
  };
}
